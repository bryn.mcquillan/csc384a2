#Look for #IMPLEMENT tags in this file. These tags indicate what has
#to be implemented to complete the warehouse domain.  

'''
Construct and return Tenner Grid CSP models.
'''

from cspbase import *
import itertools

def tenner_csp_model_1(initial_tenner_board):
    '''Return a CSP object representing a Tenner Grid CSP problem along 
       with an array of variables for the problem. That is return

       tenner_csp, variable_array

       where tenner_csp is a csp representing tenner grid using model_1
       and variable_array is a list of lists

       [ [  ]
         [  ]
         .
         .
         .
         [  ] ]

       such that variable_array[i][j] is the Variable (object) that
       you built to represent the value to be placed in cell i,j of
       the Tenner Grid (only including the first n rows, indexed from 
       (0,0) to (n,9)) where n can be 3 to 8.
       
       
       The input board is specified as a pair (n_grid, last_row). 
       The first element in the pair is a list of n length-10 lists.
       Each of the n lists represents a row of the grid. 
       If a -1 is in the list it represents an empty cell. 
       Otherwise if a number between 0--9 is in the list then this represents a 
       pre-set board position. E.g., the board
    
       ---------------------  
       |6| |1|5|7| | | |3| |
       | |9|7| | |2|1| | | |
       | | | | | |0| | | |1|
       | |9| |0|7| |3|5|4| |
       |6| | |5| |0| | | | |
       ---------------------
       would be represented by the list of lists
       
       [[6, -1, 1, 5, 7, -1, -1, -1, 3, -1],
        [-1, 9, 7, -1, -1, 2, 1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 0, -1, -1, -1, 1],
        [-1, 9, -1, 0, 7, -1, 3, 5, 4, -1],
        [6, -1, -1, 5, -1, 0, -1, -1, -1,-1]]
       
       
       This routine returns model_1 which consists of a variable for
       each cell of the board, with domain equal to {0-9} if the board
       has a -1 at that position, and domain equal {i} if the board has
       a fixed number i at that cell.
       
       model_1 contains BINARY CONSTRAINTS OF NOT-EQUAL between
       all relevant variables (e.g., all pairs of variables in the
       same row, etc.).
       model_1 also constains n-nary constraints of sum constraints for each 
       column.
    '''
    vars = {}
    domain = [i for i in range(0,10)]
    i,j = 0,0

    #CREATE VARIABLES

    n_grid = initial_tenner_board[0]
    for row in n_grid:
        for _ in row:
            if n_grid[i][j] == -1:
                vars[str(i) + str(j)] = Variable(str(i) + str(j), domain)
            else:
                var = Variable(str(i) + str(j), [n_grid[i][j]])
                vars[str(i) + str(j)] = var
            j += 1
        j = 0
        i += 1

    csp = CSP("tenner", vars.values())

    sat_tups = [(i,j) for i in range(10) for j in range(10) if i != j]

    #ADD BINARY NOT EQUALS CONSTRAINTS

    cons = []
    for i in range(len(n_grid)):
        for j in range(10):
            for k in range(j+1, len(n_grid[0])):
                two_vars = [vars[str(i)+str(j)], vars[str(i)+str(k)]]
                con = Constraint(str(i)+str(j)+"=/="+str(i)+str(k), two_vars)
                con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                cons.append(con)
            if i != len(n_grid) -1:
                two_vars = [vars[str(i) + str(j)], vars[str(i+1) + str(j)]]
                con = Constraint(str(i) + str(j) + "=/=" + str(i+1) + str(j), [vars[str(i) + str(j)], vars[str(i+1) + str(j)]])
                con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                cons.append(con)
                if j != len(n_grid[0]) - 1:
                    two_vars = [vars[str(i) + str(j)], vars[str(i + 1) + str(j+1)]]
                    con = Constraint(str(i) + str(j) + "=/=" + str(i + 1) + str(j+1),
                                     [vars[str(i) + str(j)], vars[str(i + 1) + str(j+1)]])
                    con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                    cons.append(con)
                if j != 0:
                    two_vars = [vars[str(i) + str(j)], vars[str(i + 1) + str(j - 1)]]
                    con = Constraint(str(i) + str(j) + "=/=" + str(i + 1) + str(j - 1),
                                     [vars[str(i) + str(j)], vars[str(i + 1) + str(j - 1)]])
                    con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                    cons.append(con)
    for con in cons:
        csp.add_constraint(con)


    #ADD SUM CONTRAINTS
    last_row = initial_tenner_board[1]
    i = 0
    for col in last_row:
        sat_tups = []
        scope = [vars[str(j) + str(i)] for j in range(len(n_grid))]
        for t in itertools.product(range(10), repeat = len(n_grid)):
            if sum(t) == col and sum_tups_filter(t, scope):
                sat_tups.append(t)
        con = Constraint(str(i) + str(col), scope)
        con.add_satisfying_tuples(sat_tups)
        csp.add_constraint(con)
        i += 1

    vararray = [[vars[str(i) +str(j)] for j in range(len(n_grid[0]))] for i in range(len(n_grid))]

    return csp, vararray



#IMPLEMENT


def bin_tups_filter(sat_tups, two_vars):
    if len(two_vars[0].domain()) == 1 and len(two_vars[1].domain()) == 1:
        return [x for x in sat_tups if x[0] == two_vars[0].domain()[0] and x[1] == two_vars[1].domain()[0]]
    elif len(two_vars[0].domain()) == 1:
        return [x for x in sat_tups if x[0] == two_vars[0].domain()[0]]
    elif len(two_vars[1].domain()) == 1:
        return [x for x in sat_tups if x[1] == two_vars[1].domain()[0]]
    else:
        return sat_tups

def sum_tups_filter(tup, scope):
    for j, i in enumerate(scope):
        if len(i.domain()) == 1 and tup[j] != i.domain()[0]:
            return False

    return True

##############################

def tenner_csp_model_2(initial_tenner_board):
    '''Return a CSP object representing a Tenner Grid CSP problem along 
       with an array of variables for the problem. That is return

       tenner_csp, variable_array

       where tenner_csp is a csp representing tenner using model_1
       and variable_array is a list of lists

       [ [  ]
         [  ]
         .
         .
         .
         [  ] ]

       such that variable_array[i][j] is the Variable (object) that
       you built to represent the value to be placed in cell i,j of
       the Tenner Grid (only including the first n rows, indexed from 
       (0,0) to (n,9)) where n can be 3 to 8.

       The input board takes the same input format (a list of n length-10 lists
       specifying the board as tenner_csp_model_1.
    
       The variables of model_2 are the same as for model_1: a variable
       for each cell of the board, with domain equal to {0-9} if the
       board has a -1 at that position, and domain equal {i} if the board
       has a fixed number i at that cell.

       However, model_2 has different constraints. In particular,
       model_2 has a combination of n-nary 
       all-different constraints and binary not-equal constraints: all-different 
       constraints for the variables in each row, binary constraints for  
       contiguous cells (including diagonally contiguous cells), and n-nary sum 
       constraints for each column. 
       Each n-ary all-different constraint has more than two variables (some of 
       these variables will have a single value in their domain). 
       model_2 should create these all-different constraints between the relevant 
       variables.
    '''

    vars = {}
    domain = [i for i in range(0,10)]
    i,j = 0,0

    #ADD VARIABLES

    n_grid = initial_tenner_board[0]
    for row in n_grid:
        for _ in row:
            if n_grid[i][j] == -1:
                vars[str(i) + str(j)] = Variable(str(i) + str(j), domain)
            else:
                var = Variable(str(i) + str(j), [n_grid[i][j]])
                vars[str(i) + str(j)] = var
            j += 1
        j = 0
        i += 1

    csp = CSP("tenner", vars.values())

    sat_tups = [(i,j) for i in range(10) for j in range(10) if i != j]

    #ADD ROW ALL-DIF AND ADJACENT BINARY DIFF CONSTRAINTS

    cons = []
    all_diff = [t for t in itertools.permutations(range(10), 10)]
    for i in range(len(n_grid)):
        scope = [vars[str(i) + str(m)] for m in range(10)]
        con = Constraint("row-diff " + str(i), [vars[str(i) + str(m)] for m in range(10)])
        con.add_satisfying_tuples(filter(lambda x: sum_tups_filter(x, scope), all_diff))
        cons.append(con)
        for j in range(10):
            if i != len(n_grid) -1:
                two_vars = [vars[str(i) + str(j)], vars[str(i+1) + str(j)]]
                con = Constraint(str(i) + str(j) + "=/=" + str(i+1) + str(j), [vars[str(i) + str(j)], vars[str(i+1) + str(j)]])
                con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                cons.append(con)
                if j != len(n_grid[0]) - 1:
                    two_vars = [vars[str(i) + str(j)], vars[str(i + 1) + str(j+1)]]
                    con = Constraint(str(i) + str(j) + "=/=" + str(i + 1) + str(j+1),
                                     [vars[str(i) + str(j)], vars[str(i + 1) + str(j+1)]])
                    con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                    cons.append(con)
                if j != 0:
                    two_vars = [vars[str(i) + str(j)], vars[str(i + 1) + str(j - 1)]]
                    con = Constraint(str(i) + str(j) + "=/=" + str(i + 1) + str(j - 1),
                                     [vars[str(i) + str(j)], vars[str(i + 1) + str(j - 1)]])
                    con.add_satisfying_tuples(bin_tups_filter(sat_tups, two_vars))
                    cons.append(con)
    for con in cons:
        csp.add_constraint(con)

    #ADD SUM CONSTRAINTS
    last_row = initial_tenner_board[1]
    i = 0
    for col in last_row:
        sat_tups = []
        scope = [vars[str(j) + str(i)] for j in range(len(n_grid))]
        for t in itertools.product(range(10), repeat = len(n_grid)):
            if sum(t) == col and sum_tups_filter(t, scope):
                sat_tups.append(t)
        con = Constraint(str(i) + str(col), scope)
        con.add_satisfying_tuples(sat_tups)
        csp.add_constraint(con)
        i += 1

    vararray = [[vars[str(i) +str(j)] for j in range(len(n_grid[0]))] for i in range(len(n_grid))]
    return csp, vararray



if __name__ == "__main__":
    tenner_csp_model_1(([[-1, 0, 1,-1, 9,-1,-1, 5,-1, 2],
       [-1, 7,-1,-1,-1, 6, 1,-1,-1,-1],
       [-1,-1,-1, 8,-1,-1,-1,-1,-1, 9],
       [ 6,-1, 4,-1,-1,-1,-1, 7,-1,-1],
       [-1, 1,-1, 3,-1,-1, 5, 8, 2,-1]],
      [29,16,18,21,24,24,21,28,17,27]))